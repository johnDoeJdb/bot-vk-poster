<?php

namespace app\commands;

use app\services\vkWrapper;
use yii\console\Controller;

class FeedController extends Controller
{
    public function actionIndex()
    {
        vkWrapper::checkNewsFeed();
    }
}
