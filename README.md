***Первичная настройка***

*Требование к серверу*: 
- PHP 7
- php-fpm
- php-sqlite
- php-curl
- php-mbstring 
- sqlite3
- nginx

*Nginx vhost конфиг*
```
server {
    charset utf-8;
    client_max_body_size 128M;

    listen 80;

    server_name bot-vk-poster.tk; # here provide your domain
    root        /var/www/bot-vk-poster/web;
    index       index-dev.php;

    access_log  /var/log/nginx/bot_vkposter_access.log;
    error_log   /var/log/nginx/bot_vkposter_error.log;


    location / {
        # Redirect everything that isn't a real file to index.php
        try_files $uri $uri/ /index-dev.php?$args;
    }

    location ~ \.php$ {
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root/$fastcgi_script_name;
        fastcgi_pass unix:/run/php-fpm/php-fpm.sock;
        try_files $uri =404;
    }

    location ~ /\.(ht|svn|git) {
        deny all;
    }
}
```

*Регистрация проиложения VK*:

1. Рестрируем приложение VK https://vk.com/editapp?act=create
Выбираем Standalone Application и устанавливаем его название

2. Идем в https://vk.com/apps?act=manage , кликаем manage

3. Идем в Settings 
Берем параметры и кладем config/params.php:
 Application ID -> client_id
 Secure key -> client_secret
 
4. Включить OpenAPI
4.1 Ввести адрес сайта где расположено приложение
4.2 Базовый домен где расположено приложение 

5. Access Rights:
Включить разрешение на все права (разрешение на запрос приложением всх прав), но приложение будет запрашивать только нужные.

6. Application status:
Включено

*Получение токена*:

_**http://YOUR_DOMAIN/site/registration**_
*Вместо YOUR_DOMAIN адрес (домен) сервера куда приложение было загружено

Параметр code из получение URK копируем и вставляем в новый url:
_**http://YOUR_DOMAIN/site/token?code=124b78e335f9fdf8ab**_ 

Выполнянтся один раз при установке или смене пользователя.

***Инстркция по использованию***:

Все необходимые настройки находятся в params.php.

Добавление/удаление групп происходит путем добавлени уникального имени группы (вытягивается из URL)
в массив groups в файле params.php

После добавления/удаления групп в файле params.php выполните установку новых параметров в приложении путем открытия URL:
   
_**http://YOUR_DOMAIN/site/setup**_ 
*Вместо YOUR_DOMAIN адрес (домен) сервера куда приложение было загружено

После этого будут в базу загружены ID всех группы перечисленных в params.php и установлена последняя запись на сетене как последняя, т.е. все предыдущие события до текущего момента не будут учитываться скриптом.

Интервалы времени устанавливаются через cron:
```
crontab -e

*/5 * * * * php /var/www/bot-vk-poster/yii feed
```