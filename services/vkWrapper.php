<?php

namespace app\services;

use app\models\Group;
use app\models\User;
use BW\Vkontakte;
use GuzzleHttp\Client;
use yii\helpers\ArrayHelper;

class vkWrapper
{
    public static $vk;

    public static function init()
    {
        self::$vk = new Vkontakte([
            'client_id' => \Yii::$app->params['client_id'],
            'client_secret' => \Yii::$app->params['client_secret'],
            'redirect_uri' => \Yii::$app->params['redirect_uri'],
        ]);
        self::$vk->setScope(['offline', 'wall', 'friends', 'groups']);
        $user = User::getCurrentUser();
        if ($user && $user->token) {
            self::$vk->setAccessToken($user->token);
        }
    }

    public static function getVk() : Vkontakte
    {
        if (!self::$vk) {
            self::init();
        }

        return self::$vk;
    }

    public static function checkNewsFeed() : Vkontakte
    {
        $user = User::getCurrentUser();
        $lastDate = (int) $user->last_date;
        $nextLastDate = $lastDate;

        $feedPosts = vkWrapper::getVk()->api('newsfeed.get', [
            'start_time' => $user['last_date'],
            'filters' => 'post',
            'count' => '25',
        ]);

        foreach ($feedPosts['items'] as $item) {
            $itemDate = (int) $item['date'];
            $isSelf = self::isSelfItem($item['source_id']);
            if ($isSelf) {
                continue;
            }
            if ($itemDate > $lastDate) {
                $publishDate = self::getPublishDate($user);
                $screenName = self::getScreenName(
                    $item['source_id'],
                    $feedPosts['profiles'],
                    $feedPosts['groups']
                );
                try {
                    self::handleFeedItem($item, $screenName, $publishDate);
                } catch (\Exception $exception) {}
            }
            if ($itemDate > $nextLastDate) {
                $nextLastDate = $itemDate;
            }
        }

        $user->last_date = $nextLastDate;
        $user->save();

        return self::$vk;
    }

    private static function handleFeedItem(array $post, string $screenName = null, int $publishDate)
    {
        $groupsForPost = Group::find()->asArray()->all();

        foreach ($groupsForPost as $group) {
            $attachments = [];
            $postParams = [
                'owner_id' => '-'.$group['group_id'],
            ];
            $text = '';

            if (array_key_exists('copy_history', $post)) {
                $originalPost = $post['copy_history'][0];
                if (array_key_exists('text', $originalPost)) {
                    $text = $originalPost['text'];
                }
                if (array_key_exists('attachments', $originalPost)) {
                    foreach ($originalPost['attachments'] as $attachment) {
                        if ($attachment['type'] === 'link') {
                            $attachments[] = $attachment[$attachment['type']]['url'];
                        } else {
                            $attachments[] =
                                $attachment['type'] .
                                $attachment[$attachment['type']]['owner_id'] . '_' .
                                $attachment[$attachment['type']]['id'];
                        }
                    }
                }
                if ($attachments) {
                    $postParams['attachments'] = implode(',', $attachments);
                }
            } else {
                if (array_key_exists('text', $post)) {
                    $text = $post['text'];
                }
                if (array_key_exists('attachments', $post)) {
                    foreach ($post['attachments'] as $attachment) {
                        if ($attachment['type'] === 'link') {
                            $attachments[] = $attachment[$attachment['type']]['url'];
                        } else {
                            $attachments[] =
                                $attachment['type'] .
                                $attachment[$attachment['type']]['owner_id'] . '_' .
                                $attachment[$attachment['type']]['id'];
                        }
                    }
                }
                if ($attachments) {
                    $postParams['attachments'] = implode(',', $attachments);
                }
            }

            $postParams['publish_date'] = $publishDate;

            $customText = \Yii::$app->params['prepend_post_text'];
            $sourceId = $post['source_id'];
            $postId = $post['post_id'];
            $originalUrl = "https://vk.com/$screenName?w=wall{$sourceId}_{$postId}";
            $shortUrl = self::getShortUrl($originalUrl);
            $postParams['message'] = "@$screenName $text $customText \n$shortUrl";

            $postResult = vkWrapper::getVk()->api('wall.post', $postParams, true);
        }
    }

    private static function getScreenName(int $sourceId, array $profiles, array $groups)
    {
        $sourceId = abs($sourceId);
        foreach ($profiles as $profile) {
            if ((int) $profile['id'] === $sourceId) {
                return $profile['screen_name'];
            }
        }
        foreach ($groups as $group) {
            if ((int) $group['id'] === $sourceId) {
                return $group['screen_name'];
            }
        }
    }

    private static function getShortUrl(string $url) : string
    {
        $client = new Client();
        $googleApiKey = \Yii::$app->params['google_api_key'];
        $response = $client->request('POST', "https://www.googleapis.com/urlshortener/v1/url?key={$googleApiKey}", [
            'headers'        => [
                'Content-Type' => 'application/json'
            ],
            'body' => "{\"longUrl\": \"$url\"}"
        ]);
        $json = $response->getBody()->getContents();
        $data = json_decode($json, true);

        if (array_key_exists('id', $data)) {
            return $data['id'];
        }

        return $url;
    }

    private static function getPublishDate(User &$user) : int
    {
        $user->last_publish_date = (int) $user->last_publish_date + (int) \Yii::$app->params['publish_seconds_delta'];

        return $user->last_publish_date;
    }

    private static function isSelfItem($sourceId) : bool
    {
        $groups = Group::find()
            ->select(['group_id'])
            ->asArray()
            ->all()
        ;
        $groups = ArrayHelper::map($groups, 'group_id', 'group_id');
        $groups = array_values($groups);
        if (is_numeric($sourceId)) {
            $sourceId = abs($sourceId);
        }

        return in_array((string) $sourceId, $groups);;
    }
}