<?php

namespace app\controllers;

use app\models\Group;
use app\models\User;
use app\services\vkWrapper;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

class SiteController extends Controller
{
    public function behaviors() : array
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions() : array
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function beforeAction($action)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return parent::beforeAction($action);
    }

    public function actionRegistration()
    {
        $vk = vkWrapper::getVk();
        $loginUrl = $vk->getLoginUrl();

        $this->redirect($loginUrl);
    }

    public function actionToken()
    {
        $code = \Yii::$app->request->get('code');
        $vk = vkWrapper::getVk();

        if (!$code) {
            $params = [
                'get' => \Yii::$app->request->get(),
                'post' =>  \Yii::$app->request->post(),
            ];

            $json = json_encode($params);

            return 'Code not found. Please repeat registration process';
        } else {
            $vk->authenticate($code);
            $token = $vk->getAccessToken();

            User::deleteAll(['<>','id', 0]);
            $user = new User();
            $user->token = json_encode($token);
            $saved = $user->save();

            $json = $user->token;
        }

        return json_decode($json);

        return 'Token successfully stored in db. Application ready to use.';
    }

    public function actionSetup()
    {
        Group::deleteAll(['<>','id', 0]);
        $groups = vkWrapper::getVk()->api('groups.getById', [
            'group_ids' => implode(',', \Yii::$app->params['groups']),
        ]);
        foreach ($groups as $item) {
            $group = new Group();
            $group->group_id = $item['id'];
            $group->save();
        }

        $result = vkWrapper::getVk()->api('newsfeed.get', [
            'count' => 1,
        ]);
        $lastDate = $result['items'][0]['date'];
        $user = User::find()->one();
        $user->last_date = $lastDate;
        $user->last_publish_date = \Yii::$app->params['first_publish_date'];
        $user->save();

        return $result;
    }

    public function actionTest()
    {
        $result = vkWrapper::getVk()->api('newsfeed.get', [
//            'filters' => 'post',
        ]);

        return $result;
    }

    public function actionDoCheck()
    {
        vkWrapper::checkNewsFeed();

        return 'ok';
    }
}
