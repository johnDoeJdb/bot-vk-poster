<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class Group extends ActiveRecord
{
    public static function tableName() : string
    {
        return '{{%groups}}';
    }

    public function rules() : array
    {
        return [
            [['group_id'], 'integer'],
        ];
    }

    public function attributeLabels() : array
    {
        return [
            'id' => 'ID',
            'group_id' => 'Group ID',
        ];
    }
}
