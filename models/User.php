<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class User extends ActiveRecord
{
    public static function tableName() : string
    {
        return '{{%user}}';
    }

    public function rules() : array
    {
        return [
            [['token'], 'string'],
            [['last_date'], 'integer'],
            [['last_publish_date'], 'integer'],
        ];
    }

    public function attributeLabels() : array
    {
        return [
            'id' => 'ID',
            'token' => 'Token',
            'last_id' => 'Last id',
        ];
    }

    public static function getLastPublishDate()
    {
        return self::find()->max('last_publish_date');
    }

    public static function getCurrentUser()
    {
        return self::find()->one();
    }
}
