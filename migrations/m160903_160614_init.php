<?php

use yii\db\Migration;

class m160903_160614_init extends Migration
{
    public function up()
    {
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'token' => $this->text(),
            'last_date' => $this->bigInteger(),
            'last_publish_date' => $this->bigInteger(),
        ]);
    }

    public function down()
    {
        echo "m160903_160614_init cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
