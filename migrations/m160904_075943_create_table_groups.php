<?php

use yii\db\Migration;

class m160904_075943_create_table_groups extends Migration
{
    public function up()
    {
        $this->createTable('{{%groups}}', [
            'id' => $this->primaryKey(),
            'group_id' => $this->integer(),
        ]);
    }

    public function down()
    {
        echo "m160904_075943_create_table_groups cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
